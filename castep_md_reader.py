#!/usr/bin/env python3
################################################################
#
# castep_md_reader
#   basic script that can read a CASTEP .md file in python
#
# This script reads a .md file and extracts the positions, velocities, etc.
#
# The results are stored in a List[configuration] using same units
# as in the .md file (atomic units)
#
# Also comes with functions to convert from atomic to physics units
#
################################################################
#  Based on castep2force by Corwin Kuiper 2020
#  This library module by Matt Probert 2021
#################################################################

import argparse
import gzip
import sys

from collections import OrderedDict

from typing import Dict, List, IO, Union

def fatal(message: str) -> None:
    if message.endswith("\n"):
        sys.stderr.write("FATAL: {}".format(message))
    else:
        sys.stderr.write("FATAL: {}\n".format(message))
    sys.exit(1)


def warning(message: str) -> None:
    if message.endswith("\n"):
        sys.stderr.write("WARN: {}".format(message))
    else:
        sys.stderr.write("WARN: {}\n".format(message))

def atomic_energy(hartree: str) -> float:
    #md file has energies in Hartree so convert to eV 
    return float(hartree) * 27.211386245988

def atomic_temperature(kBT: str) -> float:
    #md file has temperature in Hartree per atom so convert to K 
    return float(kBT) * 3.157750248E5

def atomic_length(bohr: str) -> float:
    #md file has lengths in Bohr so convert to Ang
    return float(bohr) * 0.529177210903

def atomic_time(aut: str) -> float:
    #md file has time in aut so convert to fs
    return float(aut) * 0.02418884326509

def atomic_velocity(velocity: str) -> float:
    #md file has velocity in Bohr per atomic-unit-of-time so convert to Ang/fs
    return float(velocity) * 0.529177210903/0.02418884326509

def atomic_force(force: str) -> float:
    #md file has forces in Hartree per Bohr so convert to eV/Ang
        return float(force) * 27.211386245988/0.529177210903

def atomic_stress(stress: str) -> float:
    #md file has stress in Harree per Bohr^3 so convert to eV/Ang^3
    return float(stress) *  27.211386245988/(0.529177210903^3)

class Atom:
    def set_position(self, x: float, y: float, z: float) -> None:
        self.r_x = x
        self.r_y = y
        self.r_z = z

    def set_velocity(self, x: float, y: float, z: float) -> None:
        self.v_x = x
        self.v_y = y
        self.v_z = z

    def set_force(self, x: float, y: float, z: float) -> None:
        self.f_x = x
        self.f_y = y
        self.f_z = z
    
    def __init__(self, atom_type: int) -> None:
        self.type = atom_type


class Box:
    def __init__(self, x: float, y: float, z: float) -> None:
        self.x = x
        self.y = y
        self.z = z
        self.v_x = 0.0   #optional cell velocities
        self.v_y = 0.0
        self.v_z = 0.0

    def set_velocity(self, vx: float, vy: float, vz: float) -> None:
        self.v_x = vx
        self.v_y = vy
        self.v_z = vz


class Configuration:
    def __init__(self) -> None:
        # For python versions less than 3.7, need to explicitly use an ordered
        # dictionary as the order in must equal the order out.
#        self.num_atoms
        self.types_of_element = OrderedDict()  # type: Dict[str, int]
        self.boxes = []  # type: List[Box]
        self.time        = None
        self.pot_energy  = None  # type: Union[None, float]
        self.ham_energy  = None  # type: Union[None, float]
        self.kin_energy  = None  # type: Union[None, float]
        self.temperature = None  # type: Union[None, float]
        self.pressure    = None

        self.stress = []  # type: List[List[float]]

        # A dictionary where the key is a string representing an element and
        # contains a list of atoms of that type.
        self.body = OrderedDict()  # type: Dict[str, List[Atom]]

def read_configuration(file: IO[str]):
    # Reads a configuration from specified file and returns a configuration object
    # or None if at EOF

    config = Configuration()
    hv_line = 0
    
    for line in file:
        stripped_line = line.strip()
        
        
        # Skip over the header
        if line.strip() == "BEGIN header":
            #print('Header skipping')
            for _ in range(4):
                #print('Header skipped')
                file.readline()
                
            continue

        # reached empty line between md steps
        if stripped_line == "":
           # print('Reached empty line between md steps')
            hv_line = 0
            break

        split_line = stripped_line.split()
        tag = split_line[len(split_line) - 1]
        
        #no '<-- tag' means the time value
        if len(tag)>2:
            config.time = split_line[0]

        if tag == "E":
            # energy, has 3 elements in it, Potential, Hamiltonian, Kinetic.
            # In most MD ensembles we are interested in the total energy
            config.pot_energy = split_line[0] #DFT total E is the first entry
            config.ham_energy = split_line[1]
            config.kin_energy = split_line[2]

        if tag == "T":
            # temperature
            config.temperature = split_line[0]

        if tag == "P":
            # pressure
            config.pressure = split_line[0]

        # Cell matrix vector
        if tag == "h":
            x = split_line[0]
            y = split_line[1]
            z = split_line[2]

            config.boxes.append(Box(x, y, z))

        # Cell matrix velocity vector
        if tag == "hv":
            vx = split_line[0]
            vy = split_line[1]
            vz = split_line[2]

            config.boxes[hv_line].set_velocity(vx,vy,vz)
            hv_line +=1
            
        # Stress matrix element
        if tag == "S":
            x = split_line[0]
            y = split_line[1]
            z = split_line[2]

            config.stress.append([x, y, z])

        # coordinate of atom
        if tag == "R":
            element_type = split_line[0]
            atom_number = int(split_line[1])
            x = split_line[2]
            y = split_line[3]
            z = split_line[4]

            # add atom type, eg Ar, to the list of types of element
            if element_type not in config.types_of_element:
                config.types_of_element[element_type] = len(config.types_of_element)
                config.body[element_type] = []
            element_type_int = config.types_of_element[element_type]

            a = Atom(element_type_int)
            a.set_position(x, y, z)
            config.body[element_type].append(a)

        # velocity of atom
        if tag == "V":
            element_type = split_line[0]
            atom_number = int(split_line[1])
            v_x = split_line[2]
            v_y = split_line[3]
            v_z = split_line[4]
            
            # atom numbers in md file start at 1
            atom_index = atom_number - 1
            config.body[element_type][atom_index].set_velocity(v_x, v_y, v_z)
                        
        # force acting on atom
        if tag == "F":
            element_type = split_line[0]
            atom_number = int(split_line[1])
            f_x = split_line[2]
            f_y = split_line[3]
            f_z = split_line[4]

            # atom numbers in md file start at 1
            atom_index = atom_number - 1
            config.body[element_type][atom_index].set_force(f_x, f_y, f_z)
            
    else:
        # test for whether file is at at EOF. else runs if for loop didn't
        # encounter break, which happens when at EOF.
        return None
    
    return config

def atomic_to_units(conf: Configuration) -> None:
    #in-place conversion of conf from atomic to physics units: eV, Ang, etc

    #scalars
    conf.time        =atomic_time(conf.time)
    conf.pot_energy  =atomic_energy(conf.pot_energy)
    conf.ham_energy  =atomic_energy(conf.ham_energy)
    conf.kin_energy  =atomic_energy(conf.kin_energy)
    conf.temperature =atomic_temperature(conf.temperature)
    if (conf.pressure):    #optional
        conf.pressure = atomic_stress(conf.pressure)
    else:
        conf.pressure = 0.0

    #cell info
    for i in range(3):
        conf.boxes[i].x=atomic_length(conf.boxes[i].x)
        conf.boxes[i].y=atomic_length(conf.boxes[i].y)
        conf.boxes[i].z=atomic_length(conf.boxes[i].z)
        
        conf.boxes[i].v_x=atomic_velocity(conf.boxes[i].v_x)
        conf.boxes[i].v_y=atomic_velocity(conf.boxes[i].v_y)
        conf.boxes[i].v_z=atomic_velocity(conf.boxes[i].v_z)
        
    #atom info
    for _,value in conf.body.items():
        for atom in value:
            atom.r_x=atomic_length(atom.r_x)
            atom.r_y=atomic_length(atom.r_y)
            atom.r_z=atomic_length(atom.r_z)
            atom.v_x=atomic_velocity(atom.v_x)
            atom.v_y=atomic_velocity(atom.v_y)
            atom.v_z=atomic_velocity(atom.v_z)
            atom.f_x=atomic_force(atom.f_x)
            atom.f_y=atomic_force(atom.f_y)
            atom.f_z=atomic_force(atom.f_z)
        
    if (len(conf.stress)>0):   #stress is optional so only convert if present
        for i in range(3):
            for j in range(3):
                conf.stress[i][j]=atomic_stress(conf.stress[i][j])

    return 

def open_file(filename: str) -> IO[str]:
    if filename == "-":
        return sys.stdin
    if filename.endswith(".gz"):
        return gzip.open(filename, "rt")
    return open(filename, "rt")


def read_md_file(filename: str) -> List[Configuration]:

    # CASTEP produces a .md files with multiple iteration steps, each one is a different configuration
    configurations = []
    i=0

    try:
        with open_file(filename) as file:
            # continually read configurations until read configuration returns
            # false, at which point file has reached eof
            while True:
                config = read_configuration(file)
                if config is None:
                    break
                configurations.append(config)
                i=i+1
                
    except FileNotFoundError:
        fatal("Cannot read file '{}' as it does not exist".format(filename))

    return configurations
