#!/usr/bin/env python3
################################################################
#
# Analyse CASTEP md trajectory file to extract phonon info
#
################################################################

import argparse
import sys
from typing import List

import numpy as np
import scipy.constants as sp_c
import scipy.signal
from scipy.fft import fft
import matplotlib.pyplot as plt

def parse_cli_flags(argv: List[str]) -> argparse.Namespace:
    parser = argparse.ArgumentParser(
        description="Analyse a CASTEP molecular dynamics trajectory file to extract phonon information",
        epilog="Example usage: ./md_to_phonons -o configuration.out mdinput.md",
    )

    parser.add_argument(
        "md_file", type=str, nargs="?",
        help="CASTEP molecular dynamics trajectory file",
    )
    
    parser.add_argument(
        "-o", "--output",type=str, required=False,
        help="Output file to write, Default is to standard output",
        default="-",
    )
    return parser.parse_args(argv)


def main(argv: List[str]) -> None:
    import castep_md_reader as md

    arguments = parse_cli_flags(argv)    #Use this version of parse
    input_filename  = arguments.md_file  #not the one in castep_md_reader\
    output_filename = arguments.output
    
    #Read a CASTEP MD file and store info in the list
    configuration = md.read_md_file(input_filename)
    if output_filename == "-" or output_filename is None:
        print ('success - read configuration OK from ',input_filename)
    else:
        with open(output_filename, "w") as file:
            file.write('Success - read configuration OK')

    # Store data into array
    # Number of configurations
    n_configuration = len(configuration)
    print ('Number of configurations are: ', n_configuration)

    # Number of atoms in each configurations for all species
    n_atoms = 0
    for _, t in configuration[0].body.items():
        n_atoms += len(t)
    print ('Number of atoms are :', n_atoms)

    # Set up a 3D array with correct sizes
    velocities = np.zeros ((3, n_atoms, n_configuration), dtype=float)
    
    # config_number = time_step
    for config_number in range (0, n_configuration, 1):
        # Number of the atom within the species 
        atom_value = 0

        # Assign value of velocities from configurations to velocities array
        for _, value in configuration[config_number].body.items(): # Set of velocity data from md file

                    for atom in value:

                        velocities[0,atom_value,config_number] = (atom.v_x)
                        velocities[1,atom_value,config_number] = (atom.v_y)
                        velocities[2,atom_value,config_number] = (atom.v_z)
                        
                        atom_value = atom_value + 1

    # Manipulate data for calculations
    # Arrays for velocity autocorrelation equation: c_v_t
    c_v_t = np.zeros((n_configuration))
    V1 = np.zeros (((n_configuration)))
    VAF2 = np.zeros((n_configuration)*2 - 1)
    
    # Calculation of c_v_t : Eq. B7
    for i in range(n_atoms):
        for j in range(3):
            for iter in range (n_configuration):
                V1[iter] = velocities[j,i,iter]
            VAF2 += scipy.signal.correlate(V1, V1, mode='full')
    # two-sided VAF
    VAF2 /=  np.sum(velocities**2)
    c_v_t = VAF2[int(VAF2.size/2):]
    print ('This is c_v_t: ' , c_v_t)

    # Fourier transform of c_v_t to obtain c_w
    # NOTE : need to implement time/(t) and temperature(T)
    c_w = fft(c_v_t)
    print ('This is c_w: ' , c_w)
    
    # Calculate g(w) = Eq. B10
    i = complex(0,1) # Complex number i = sqrt(-1)
    # Loop over to obtain converted time and temperature values for each configuration
    for config_number in range (0, n_configuration, 1):
        w = 2 * np.pi * md.atomic_time(configuration[config_number].time)
        g_w = c_w * np.exp((i * sp_c.hbar * (w**2))/(sp_c.Boltzmann * md.atomic_temperature(configuration[config_number].temperature) ) )

    # Output data
    c_v_t_output = open("c_v_t_output.txt", "w")
    np.savetxt(c_v_t_output, c_v_t)
    c_v_t_output.close()
    
    c_w_output = open("c_w_output.txt", "w")
    np.savetxt(c_w_output, c_w)
    c_w_output.close()
    
    g_w_output = open("g_w_output.txt", "w")
    np.savetxt(g_w_output, g_w)
    g_w_output.close()

    # Plot  for c_v_t - kept here in case needs for check B7
    #plt.plot (abs(c_v_t), color='red', marker='x', linestyle='dashed', linewidth=2, markersize=0, label = "Data")

    plt.xlabel ('Frequenies / $m^{-1}$')
    plt.ylabel ('Phonon density of states / arb. units')
    plt.title ("Vibrational spectrum from FFT of $c_{v}(t)$")
    
    # Plot magnitude of complex function and need half of function plotted
    plt.plot (abs(g_w), color='red', marker='x', linestyle='dashed', linewidth=2, markersize=0, label = "Data")
    plt.legend (loc ='upper right')
    plt.axis ([-50,1500,-2,80]) # Only half of graph shown as 
    plt.show()


if __name__ == "__main__":
    main(sys.argv[1:])

